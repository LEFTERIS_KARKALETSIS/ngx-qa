import { CommonModule} from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ModalService, SharedModule } from '@universis/common';
import { RouterModalModule } from '@universis/common/routing';
import { AdvancedFormsModule } from '@universis/forms';
import { QA_LOCALES } from './assets/i18n';
import { CourseOutlineEditComponent } from './components/course-outline/course-outline-edit/course-outline-edit.component';
import { environment } from './environments/environment';
import { CourseOutlineRootComponent } from './components/course-outline/course-outline-root/course-outline-root.component';
import {RouterModule} from '@angular/router';
import {BsDropdownModule, ModalModule, ProgressbarConfig, ProgressbarModule, TooltipModule} from 'ngx-bootstrap';
import {FormioAppConfig} from 'angular-formio';
import { CourseEvaluationComponent } from './components/course-evaluation/course-evaluation.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { EvaluationResultsComponent } from './components/course-evaluation/evaluation-results/evaluation-results.component';
import { NgArrayPipesModule, NgObjectPipesModule, NgPipesModule} from 'ngx-pipes';
import { EvaluationDashboardComponent } from './components/course-evaluation/evaluation-dashboard/evaluation-dashboard.component';
import {CourseEvaluationsListComponent} from './components/course-evaluation/course-evaluations-list/course-evaluations-list.component';
import {CourseOutlineComponent} from './components/course-outline/course-outline-view/course-outline-view.component';
import { InstructorOutlineTabsComponent } from './components/instructor-outline/instructor-outline-tabs/instructor-outline-tabs.component';
// tslint:disable-next-line: max-line-length
import { InstructorOutlinePublicationsComponent } from './components/instructor-outline/instructor-outline-publications/instructor-outline-publications.component';
import { NewPublicationOrWorkComponent } from './components/instructor-outline/new-publication-or-work/new-publication-or-work.component';
// import { NgPipesModule } from 'ngx-pipes';
import { InstructorOutlineGeneralComponent } from './components/instructor-outline/instructor-outline-general/instructor-outline-general.component';
import { PublicationsOverviewComponent } from './components/instructor-outline/instructor-outline-general/publications-overview/publications-overview.component';
import { WorkComponent } from './components/instructor-outline/instructor-outline-general/work/work.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    SharedModule,
    AdvancedFormsModule,
    RouterModalModule,
    RouterModule,
    ModalModule,
    ProgressbarModule,
    InfiniteScrollModule,
    TooltipModule,
    NgArrayPipesModule,
    NgObjectPipesModule,
    BsDropdownModule,
    NgPipesModule
  ],
  declarations: [
    CourseOutlineComponent,
    CourseOutlineEditComponent,
    CourseOutlineRootComponent,
    CourseEvaluationComponent,
    EvaluationResultsComponent,
    EvaluationDashboardComponent,
    CourseEvaluationsListComponent,
    InstructorOutlineTabsComponent,
    InstructorOutlinePublicationsComponent,
    NewPublicationOrWorkComponent,
    InstructorOutlineGeneralComponent,
    PublicationsOverviewComponent,
    WorkComponent
  ],
  exports: [
    CourseOutlineComponent,
    CourseEvaluationComponent,
    EvaluationDashboardComponent,
    CourseEvaluationsListComponent
  ],
  providers: [
    ModalService,
    ProgressbarConfig,
    {
      provide: FormioAppConfig,
      useValue: {}
    }
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [
    NewPublicationOrWorkComponent
  ]
})
export class QaModule {
  constructor( private _translateService: TranslateService) {
    this.ngOnInit();
  }

  ngOnInit() {
    environment.languages.forEach( language => {
      if (QA_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, QA_LOCALES[language], true);
      }
    });
  }
}
