/*
 * Public API Surface of universis-qa
 */

export * from './qa.service';
export {QaModule} from './qa.module';

// export * from './advanced-table.formatters';
export * from './components/course-evaluation/course-evaluation.component';
export * from './components/course-evaluation/course-evaluations-list/course-evaluations-list.component';
export * from './components/course-evaluation/evaluation-dashboard/evaluation-dashboard.component';
export * from './components/course-evaluation/evaluation-results/evaluation-results.component';
export * from './components/course-outline/course-outline-root/course-outline-root.component';
export * from './components/course-outline/course-outline-edit/course-outline-edit.component';
export * from './components/course-outline/course-outline-view/course-outline-view.component';
export * from './components/instructor-outline/instructor-outline-general/instructor-outline-general.component';
export * from './components/instructor-outline/instructor-outline-publications/instructor-outline-publications.component';
export * from './components/instructor-outline/instructor-outline-tabs/instructor-outline-tabs.component';
export * from './components/instructor-outline/new-publication-or-work/new-publication-or-work.component';
