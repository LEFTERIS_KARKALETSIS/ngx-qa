import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ErrorService } from '@universis/common';
import { QaService } from '../../../qa.service';

@Component({
  selector: 'universis-instructor-outline-tabs',
  templateUrl: './instructor-outline-tabs.component.html',
  styleUrls: ['./instructor-outline-tabs.component.scss']
})
export class InstructorOutlineTabsComponent implements OnInit {
  public isLoading: boolean;
  public instructor: any;

  constructor(private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _qaService: QaService,
              private _appEventService: AppEventService) { }

  async ngOnInit() {
    try {
      this.instructor = await this._qaService.getInstructor();
      await this.loadData();

      // reload data if a work/publication is updated, added or deleted in the work/publication component
      this._appEventService.change.subscribe(async change => {
        if (change && change.model === 'instructorOutline') {
          await this.loadData();
        }
      });

      this.isLoading = false;
    } catch (err) {
      // use error service to navigate to error method
      return this._errorService.navigateToError(err);
    }
  }

  async loadData() {
    let instructorOutline = await this._context.model('qa/Instructors/me/InstructorOutline')
      .asQueryable()
      .select('id,instructorWorks,publications')
      .expand('instructorWorks($select=instructorOutline, sum(totalWork) as total;$groupby=instructorOutline),'
        + 'publications($select=instructorOutline, count(instructorOutline) as total;$groupby=instructorOutline)')
      .getItem();

    // initialize instructor outline if none is found
    if (!instructorOutline) {
      instructorOutline = await this._context.model('qa/Instructors/me/InstructorOutline').save({});
    }

    // the following info are used in the instructor-outline-general-component
    this._appEventService.change.next({
      // tslint:disable-next-line: max-line-length
      publicationsCount: instructorOutline.publications && instructorOutline.publications.length ? instructorOutline.publications[0].total : 0,
      // tslint:disable-next-line: max-line-length
      worksSum: instructorOutline.instructorWorks && instructorOutline.instructorWorks.length ? instructorOutline.instructorWorks[0].total : 0,
      outlineId: instructorOutline.id
    });
  }
}
