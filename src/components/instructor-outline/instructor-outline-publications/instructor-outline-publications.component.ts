import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ErrorService, LoadingService, ToastService } from '@universis/common';
import { QaService } from '../../../qa.service';

@Component({
  selector: 'universis-instructor-outline-publications',
  templateUrl: './instructor-outline-publications.component.html',
  styleUrls: ['./instructor-outline-publications.component.scss']
})
export class InstructorOutlinePublicationsComponent implements OnInit {
  public publications: any = [];
  public academicYears: any = [];
  public selectedYear: any;
  public searchText = '';
  public isLoading = true;
  public collapsedCards = {};

  constructor(private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _toastService: ToastService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _appEventService: AppEventService,
              private _qaService: QaService) { }

  async ngOnInit() {
    try {
      // get all academic years and a publication count for each year
      this.academicYears = await this.getAcademicYears();
      // set the most recent year as the selected year
      // if no academic years are found (0 publications), set "all years" choice as the selected year
      this.selectedYear = this.academicYears[0] || -1;
      await this.getPublications();
      this.isLoading = false;
    } catch (err) {
      // use error service to navigate to error method
      return this._errorService.navigateToError(err);
    }
  }

  async reloadData() {
    // this function is called if a publication is added or deleted
    // call getAcademicYears() again to update publication counts
    this.academicYears = await this.getAcademicYears();
    // if not choice "all years" (-1) is selected, update selectedYear cause selectedYear.total has changed
    // if find returns null (all pubs for the selectedYear were deleted), set the most recent year or "all years" as the selected year
    if (this.selectedYear !== -1) {
      const updatedYear = this.academicYears.find(x => x.year === this.selectedYear.year);
      this.selectedYear = updatedYear || this.academicYears[0] || -1;
    }

    await this.getPublications();
    this.reloadTabsComponent();
    this.isLoading = false;
  }

  async getPublications() {
    this._loadingService.showLoading();
    // cards should not be collapsed when data is loaded/reloaded
    for (const key of Object.keys(this.collapsedCards)) {
      this.collapsedCards[key] = false;
    }

    // prepare query
    const query = this._context.model('qa/Instructors/me/Publications')
      .asQueryable()
      .where('title').contains(this.searchText)
      .or('authors').contains(this.searchText)
      .or('identifier').contains(this.searchText)
      .or('publicationType/name').contains(this.searchText)
      .or('source/name').contains(this.searchText)
      .expand('publicationType($expand=locale),source')
      .orderBy('publicationType/name')
      .take(-1)
      .prepare();

    // if a year is selected, extend the query's filter
    if (this.selectedYear && this.selectedYear.year) {
      query.and('year').equal(this.selectedYear.year);
    }

    try {
    // get publications
      this.publications = await query.getItems();
    } catch (err) {
      // use error service to navigate to error method
      return this._errorService.navigateToError(err);
    }

    this._loadingService.hideLoading();
  }

  async addOrEditPublication(id) {
    // if an id is given, the corresponding publication is loaded so that the user can update the record
    // else a new publication will be created
    const data = id > 0 ? this.publications.find(pub => pub.id === id) : null;
    this._qaService.showPublicationOrWorkModal({
      data: data,
      formName: 'Publication',
      endpoint: 'Publications',
      toastBodySuccess: 'InstructorOutline.Publication.SavedSuccess',
      reloadData: this.reloadData.bind(this)
    });
  }

  getAcademicYears() {
    return this._context.model('qa/Instructors/me/Publications')
      .asQueryable()
      .select('year, count(id) as total')
      .groupBy('year')
      .orderByDescending('year')
      .take(-1)
      .getItems();
  }

  deletePublication(id) {
    this._loadingService.showLoading();
    this._context.model('qa/Instructors/me/Publications').save({ id: id, '$state': 4 })
    .then(() => this.reloadData())
    .then(() => {
      this._loadingService.hideLoading();
      this._toastService.show(
        this._translateService.instant('InstructorOutline.CompletedSuccess'),
        this._translateService.instant('InstructorOutline.Publication.DeletedSuccess'),
        true,
        3000
      );
    })
    .catch(err => {
      // use error service to navigate to error method
      this._loadingService.hideLoading();
      this._errorService.navigateToError(err);
    });
  }

  toggleCollapse(id) {
    if (id in this.collapsedCards) {
      this.collapsedCards[id] = !this.collapsedCards[id];
    } else {
      this.collapsedCards[id] = true;
    }
  }

  /* triggers change in instructor-outline-tabs*/
  reloadTabsComponent() {
    this._appEventService.change.next({
      model: 'instructorOutline'
    });
  }

}
