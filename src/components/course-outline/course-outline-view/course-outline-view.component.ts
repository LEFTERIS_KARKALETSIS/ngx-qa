import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ErrorService, LoadingService } from '@universis/common';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { QaService } from '../../../qa.service';

@Component({
  selector: 'universis-course-outline',
  templateUrl: './course-outline-view.component.html',
  styleUrls: ['./course-outline-view.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CourseOutlineComponent implements OnInit, OnDestroy {
  @Input() courseClass: any;
  public data: any;
  public courseOutlineReports: any;
  public instructorIsSupervisor = false;
  public isLoading = true;
  private dataSubscription: Subscription;

  constructor(private _qaService: QaService,
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) {
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

 async ngOnInit() {
     this._activatedRoute.parent.data.subscribe(async data => {
       try {
         this._loadingService.showLoading();
         this.courseClass = data.courseClass;
         // fetch the Course Outline of the specified courseClass and the corresponding report templates
         this.data = await this._qaService.getCourseOutlineSummary(this.courseClass.id);
         this.courseOutlineReports = await this._qaService.getReportTemplates('CourseClass');
         this.instructorIsSupervisor = this.data && this.data.courseClass && this.data.courseClass.instructors
           && this.data.courseClass.instructors.length;
         this._loadingService.hideLoading();
         this.isLoading = false;
       } catch (err) {
         // show error
         this._loadingService.hideLoading();
         this._errorService.showError(err, {
           continueLink: '../'
         });
       }
     });
  }


  async printCourseOutline(report) {
    try {
      // print report by passing report parameters
      const blob = await this._qaService.printReport(report.id, {
        ID: this.courseClass.id,
        REPORT_USE_DOCUMENT_NUMBER: false
      });
      const objectUrl = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = objectUrl;
      const name = `${report.name}.pdf`;
      a.download = name;
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(blob, name);
      } else {
        a.click();
      }
      window.URL.revokeObjectURL(objectUrl);
      a.remove(); // remove the element
    } catch (err) {
      console.log(err);
      return this._errorService.navigateToError(err);
    }
  }
}
