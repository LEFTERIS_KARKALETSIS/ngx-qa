/* tslint:disable quotemark */
/* tslint:disable max-line-length */
export const en = {
  "CourseOutline": {
    "Title": "Course Outline",
    "More": "More",
    "Less": "Less",
    "GeneralInfo": "General Information",
    "LearningOutcome": "Learning Outcomes",
    "Content": "Course Content",
    "Edit": "Edit",
    "Create": "Create",
    "Print": "Print",
    "Found": "Course outline has been registered. You can either modify or print it.",
    "Creation": "Creation date:",
    "Modification": "Modification date:",
    "NotFoundUnauthorized": "No course outline has been registered. A course outline can be registered only by the supervisor of the course. If you are the supervisor please contact your secretariat.",
    "NotFoundAuthorized": "No course outline has been registered. Press \"create\" button.",
    "Help": "The Course Outline includes fields pre-filled by the Secretariat's Information System (e.g. ects...) and fields that must be filled in by the person in charge of the course. Completion of all fields is mandatory in order to be submitted to the system. After submitting the Course Outline, you can edit it to modify-update the registered information."
  },
  "Evaluations" : {
    "Title" : "Evaluation",
    "TitlePlural" : "Evaluations",
    "NotFound": "Not Found",
    "Empty": "No record found",
    "NotFoundPlural": "No records found",
    "More": "More",
    "StartDate" : "Start Date",
    "EndDate" : "End Date",
    "EventStatus" : "Status",
    "Results": {
      "Title": "Results",
      "AnswerType": "Answer Type",
      "Average": "Average",
      "StandardDeviation": "Standard Deviation",
      "Question": "Question",
      "Answer": "Answer",
      "IsNumber": "Number (based on grading scale)",
      "None": {
        "Title": "Evaluation Results",
        "Message": "There are no results for this evaluation. Please visit this page when at least one of the students has used their token."
      },
      "Info": "Below you can see the results of the instructor's course evaluation by the students, up until now. For questions for which the answers were given based on a numeric scale, their average and standard deviation values are presented.",
      "NoResponses": "There are no responses.",
      "Total" : "Total",
      "Sent" : "Sent",
      "Used" : "Used"
    },
    "SearchDescription": "Search by Course Title, Code, Year or Period",
    "SearchPlaceHolder": "Course Title or code",
    "SelectYear": "Select Year",
    "SelectPeriod": "Select Period",
    "Search": "Search"
  },
  "EventStatusType" : {
    "EventOpened": "Opened",
    "EventPostponed": "Postponed",
    "EventRescheduled": "Rescheduled",
    "EventScheduled": "Scheduled",
    "EventCancelled": "Cancelled",
    "EventCompleted": "Completed"
  },
  "InstructorOutline": {
    "Title": "Instructor Outline",
    "Body": "The instructor's outline represents the research work of an instructor. You should add your works and publications and then you may print your outline.",
    "General": "General",
    "Publications": "Publications",
    "Year": "Year",
    "ReferenceYear": "Reference year",
    "Search": "Search",
    "Add": "Add",
    "Delete": "Delete",
    "Edit": "Edit",
    "CompletedSuccess": "Successful completion",
    "Total": 'Total',
    "AllYears": "All",
    "Publication": {
      "Total": "Total publications",
      "SearchPlaceHolder": "Title, source, authors, etc.",
      "Singular": "Publication",
      "Plural": "Publications",
      "SavedSuccess": "The publication was saved successfully",
      "DeletedSuccess": "The publication was deleted successfully",
      "Title": "Title",
      "Type": "Type",
      "Authors": "Authors",
      "Source": "Source",
      "Year": "Year",
      "Identifier": "Identifier",
      "NotFound": "No publications found",
    },
    "Work": {
      "Total": "Total works",
      "Singular": "Research Work",
      "SavedSuccess": "The work was saved successfully",
      "DeletedSuccess": "The work was deleted successfully",
      "NotFound": "No works found",
      "Year": "Year"
    }
  }

};
